variable "prefix_name" {}

variable "mysql_root_password" {
  default = "rootpassword"
}

variable "mysql_database" {
  default = "wordpress"
}

variable "mysql_user" {
  default = "exampleuser"
}

variable "mysql_password" {
  default = "examplepass"
}
