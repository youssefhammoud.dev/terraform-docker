terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
  required_version = ">= 0.13"
}

resource "docker_volume" "wp_vol_db" {
  name = "${var.prefix_name}-wp-vol-db"
}

resource "docker_network" "private_network" {
  name = "${var.prefix_name}-wp-net"
}

resource "docker_container" "db" {
  name  = "${var.prefix_name}-db"
  image = "mariadb"
  restart = "always"
  network_mode = "${var.prefix_name}-wp-net"
  mounts {
    type = "volume"
    target = "/var/lib/mysql"
    source = "${var.prefix_name}-wp-vol-db"
  }
  env = [
     "MYSQL_ROOT_PASSWORD=${var.mysql_root_password}",
     "MYSQL_DATABASE=${var.mysql_database}", 
     "MYSQL_USER=${var.mysql_user}",
     "MYSQL_PASSWORD=${var.mysql_password}"
  ]
}