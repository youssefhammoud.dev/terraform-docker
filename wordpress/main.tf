terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
  required_version = ">= 0.13"
}

resource "docker_volume" "wp_vol_html" {
  name = "${var.prefix_name}-wp-vol-html"
}

resource "docker_container" "wordpress" {
  name  = "${var.prefix_name}-wordpress"
  image = "wordpress:latest"
  restart = "always"
  network_mode = "${var.docker_network_name}"
  env = [
    "WORDPRESS_DB_HOST=${var.prefix_name}-db",
    "WORDPRESS_DB_USER=exampleuser",
    "WORDPRESS_DB_PASSWORD=examplepass",
    "WORDPRESS_DB_NAME=wordpress"
  ]
  ports {
    internal = "80"
    external = "${var.external_port}"
  }
  mounts {
    type = "volume"
    target = "/var/www/html"
    source = "${var.prefix_name}-wp-vol-html"
  }
}
